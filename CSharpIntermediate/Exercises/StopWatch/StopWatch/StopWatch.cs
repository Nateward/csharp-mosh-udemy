﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StopWatch
{
    public class StopWatch
    {
        private List<TimeSpan> TimeStamps = new List<TimeSpan>();
        private DateTime start;
        private DateTime stop;
        public bool _isStarted { get; private set; }

        public DateTime Start
        {
            get
            {
                return start;
            }
            set
            {
                if (!_isStarted)
                {
                    _isStarted = true;
                    start = value;
                }

            }
        }

        public DateTime Stop
        {
            get
            {
                return stop;
            }
            set
            {
                if (_isStarted)
                {
                    _isStarted = false;
                    stop = value;
                }

            }

        }

        public List<TimeSpan> TimeStampsGetSet
        {
            get
            {
                return TimeStamps;
            }

            set
            {
                TimeStamps = value;
            }
        }
    }
}
