﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace StopWatch
{
    class StopWatchBLL
    {
        public StopWatch NewStopWatch;
        public StopWatchBLL()
        {
            NewStopWatch = new StopWatch();
            StartStopWatch();
        }

        private void StartStopWatch()
        {
            var input = "";
            var StopWatchStatus = false;
            

            while (!StopWatchStatus)
            {
                var StartStopMessage = String.Format(@"Press enter to {0} the stopwatch, type quit to exit and see times.", (NewStopWatch._isStarted) ? "STOP" : "START");
                input = GetUserInput(StartStopMessage);
                StopWatchStatus = IsUserQuiting(input);
                if (!StopWatchStatus)
                {
                    StartOrStop();
                }



            }
            StartOrStop(StopWatchStatus);
            PrintTimes();
        }

        private void StartOrStop()
        {
            if (NewStopWatch._isStarted)
            {
                NewStopWatch.Stop = DateTime.Now;
                NewStopWatch.TimeStampsGetSet.Add(GetTimeSpan(NewStopWatch));
            }
            else
            {
                NewStopWatch.Start = DateTime.Now;
            }
        }

        private void StartOrStop(bool status)
        {
            if (NewStopWatch._isStarted && status)
            {
                NewStopWatch.Stop = DateTime.Now;
                NewStopWatch.TimeStampsGetSet.Add(GetTimeSpan(NewStopWatch));
            }
        }

        private bool IsUserQuiting(string input)
        {
            return String.Equals("quit", input, StringComparison.InvariantCultureIgnoreCase);
        }

        private string GetUserInput(string message)
        {
            Console.WriteLine(message);

            return Console.ReadLine();


        }

        public static TimeSpan GetTimeSpan(StopWatch times)
        {
            return times.Stop - times.Start;
        }

        private void PrintTimes()
        {
            int id = 1;
            foreach (var timaspan in NewStopWatch.TimeStampsGetSet)
            {
                Console.WriteLine("Timespan {0}: {1}", id, timaspan);
                id++;
            }
        }




    }
}
