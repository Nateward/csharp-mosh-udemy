﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            var newStack = new Stack();

            var T = new Object();

            T = "A";

            int B = 9;

            string C = "C";

            bool D = true;

            newStack.Push(T);
            newStack.Push(B);
            newStack.Push(C);
            newStack.Push(D);

            Console.WriteLine(newStack.Pop().ToString());
            Console.WriteLine(newStack.Pop().ToString());
            Console.WriteLine(newStack.Pop().ToString());
            Console.WriteLine(newStack.Pop().ToString());

            Console.ReadLine();
            
            newStack.Push(C);
            newStack.Push(D);

            newStack.Clear();

            Console.ReadLine();

           
        }
    }
}
