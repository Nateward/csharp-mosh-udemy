﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackExercise
{
    class Stack
    {
        
        private readonly List<Object> _stackItem = new List<Object>();

        public void Push(Object t)
        {
            if (t == null)
            {
                throw new InvalidOperationException();
            }
            else
            {
                Object Item = new Object();

                Item = t;

                _stackItem.Add(t);
            }

        }

        public Object Pop()
        {
            if (_stackItem.Count == 0 || _stackItem.Equals(null))
            {
                throw new InvalidOperationException();
            }
            else
            {
                var index = _stackItem.Count - 1;
                var returnItem = _stackItem[index];
                _stackItem.RemoveAt(index);

                return returnItem;
            }
        }

        public void Clear()
        {
            _stackItem.Clear();
        }
    }
}
