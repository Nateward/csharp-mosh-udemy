﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowExercise.Controller;
using WorkFlowExercise.DAO;

namespace WorkFlowExercise
{
    class Program
    {
        static void Main(string[] args)
        {

            WorkFlowEngine workFlow = new WorkFlowEngine();
            workFlow.RegisterWorkFlow(new UploadVideo());
            workFlow.RegisterWorkFlow(new ThirdPartyEncode());
            workFlow.RegisterWorkFlow(new NotifiyUploader());
            workFlow.RegisterWorkFlow(new VideoStatus());
            workFlow.Run();
        }
    }
}
