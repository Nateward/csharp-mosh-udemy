﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkFlowExercise.DAO;

namespace WorkFlowExercise.Controller
{
    public class WorkFlowEngine
    {
        private readonly List<IActivity> _tasks;

        public WorkFlowEngine()
        {
            this._tasks = new List<IActivity>();
        }

        public void Run()
        {

            foreach (var WorkFlowActivity in _tasks)
            {
                Console.WriteLine();
                var ActivityName = WorkFlowActivity.GetType().FullName.ToString();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Beginning {0}", ActivityName);
                var status = WorkFlowActivity.Exectue();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("{0} passed: {1}", ActivityName, status);
                Console.WriteLine();
                Console.ResetColor();

            }
        }

        public void RegisterWorkFlow(IActivity activity)
        {
            _tasks.Add(activity);
        }
    }
}
