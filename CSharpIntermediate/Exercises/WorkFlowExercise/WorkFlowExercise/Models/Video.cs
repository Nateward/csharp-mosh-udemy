﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowExercise.Models
{
    public class Video
    {
        public string VideoName { get; set; }
        public string UploadUser { get; set; }
        public int VideoId { get; set; }
        public DateTime UploadDate { get; set; }
    }
}
