﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowExercise.Models
{
    public class User
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }

    }
}
