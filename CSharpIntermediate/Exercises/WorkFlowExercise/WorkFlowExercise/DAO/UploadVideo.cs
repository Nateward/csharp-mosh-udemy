﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowExercise.DAO
{
    public class UploadVideo : IActivity
    {
        public bool Exectue()
        {
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Beginning Video Upload...");
            Console.ResetColor();

            return true;
        }
    }
}
