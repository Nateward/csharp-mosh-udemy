﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowExercise.DAO
{
    public class NotifiyUploader : IActivity
    {
        public bool Exectue()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Notification Email Being Sent to Uploader....");
            Console.ResetColor();

            return true;
        }
    }
}
