﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBConnection.Model;

namespace DBConnection.Controller
{
    class DbCommand
    {
        string command;
        private DbConnection ConnectionType;


        public DbCommand(DbConnection ConnectionType, string command)
        {

            this.ConnectionType = ConnectionType;
            if (!String.IsNullOrWhiteSpace(command))
            {
                this.command = command;
            }
            else
            {
                throw new NullReferenceException("No query was passed");
            }

            Execute();
        }

        private void Execute()
        {
            ConnectionType.OpenConnection();
            Console.WriteLine("DB was queried using {0}", command);
            ConnectionType.CloseConnection();
        }
    }
}
