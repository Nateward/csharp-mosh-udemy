﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConnection.Model
{
    class OracleConnection : DbConnection
    {
        public OracleConnection(string connectionString, int timespan) : base(connectionString, timespan)
        {
            
        }

        public override void CloseConnection()
        {
            Console.WriteLine("Oracle Connection Closed");
        }

        public override void OpenConnection()
        {
            Console.WriteLine("Oracle Connection Open");

        }
    }
}
