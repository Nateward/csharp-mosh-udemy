﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConnection.Model
{
    public class SqlConnection : DbConnection
    {
        public SqlConnection(string connectionString, int timespan) : base(connectionString, timespan)
        {
            
        }

        public override void CloseConnection()
        {
            Console.WriteLine("SQL Connection Closed");
        }

        public override void OpenConnection()
        {
            Console.WriteLine("SQL Connection Open");

        }
    }
}
