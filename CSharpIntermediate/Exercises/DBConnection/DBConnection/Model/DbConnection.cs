﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConnection.Model
{
    public abstract class DbConnection
    {
        string connectionString;
        public TimeSpan TimeOut;

        public DbConnection(string connectionString, int timespan)
        {
            if (!String.IsNullOrWhiteSpace(connectionString))
            {
                this.connectionString = connectionString;
            }
            else
            {
                throw new NullReferenceException("The value null was passed to the connection string. you must provide a valid connection string");
            }

            TimeOut = DateTime.Now.AddMilliseconds(timespan) - DateTime.Now;

        }


        public abstract void OpenConnection();

        public abstract void CloseConnection();

    }
}
