﻿using StackOverflowPost.BLL;
using StackOverflowPost.ConsoleUI;
using StackOverflowPost.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackOverflowPost.Controller
{
    class StackOverflowPostController
    {
        public PostCRUD PostList = new PostCRUD();
        public IO io = new IO();

        public void Run()
        {
            int userInput = 0;
            while (userInput != 5)
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();
                PrintMenu();

                Console.WriteLine();
                listNumberOfPosts();
                Console.WriteLine();
                Console.WriteLine();

                userInput = io.getInteger("Please choose from the menu options: ", 1, 5);

                switch (userInput)
                {
                    case 1:
                        addPost();
                        break;
                    case 2:
                        printPostByTitle();
                        break;
                    case 3:
                        ViewAllPosts();
                        break;
                    case 4:
                        removePost();
                        break;
                    default:
                        break;
                }
            }
        }

        private void removePost()
        {
            Post post;
            Console.WriteLine();
            Console.WriteLine("Find a post to delete:");
            var userIput = io.getInteger("1. Select a post from the list \n2. Search for post by title", 1, 2);
            Console.WriteLine();

            if (userIput == 1)
            {
                post = SelectPost(PostList.FindAllPosts());
            Console.WriteLine();
            }
            else
            {
                post = getPostByTitleFromUser("Please enter the title of the post you would like to delete: ");
            Console.WriteLine();
            }

            Console.WriteLine();

            var ViewPost = ApproveChanges("Want to view this post first, Y/N?: ", "y");
            Console.WriteLine();

            if (ViewPost)
            {
                PreviewPost(post);
            }

            Console.WriteLine();
            var deletePost = ApproveChanges("Do you want to delete this post, Y/N?: ", "y");
            Console.WriteLine();

            if (deletePost)
            {
            Console.WriteLine();
                var ReallyWantToDelete = ApproveChanges("Are you sure you want to delete this post, Y/N?: ", "y");
                Console.WriteLine();

                if (ReallyWantToDelete)
                {
                    PostList.DeletePost(post.PostId);
                }
            }
        }

        private Post getPostByTitleFromUser(string message)
        {
            Console.WriteLine();
            var UserInput = io.getString(message);
            Console.WriteLine();
            var posts = PostList.FindPostByTitle(UserInput);

            if (posts.Count() > 0)
            {
                if (posts.Count == 1)
                {
                    return posts[0];
                }
                else
                {
            Console.WriteLine();
                    Console.WriteLine("More tha one entry found....");
            Console.WriteLine();
            Console.WriteLine();
                    return SelectPost(posts);
                }
            }
            else
            {
                return null;
            }
        }

        private Post SelectPost(List<Post> posts)
        {
            for (int i = 0; i < posts.Count; i++)
            {
            Console.WriteLine();
                Post post = posts[i];
                Console.WriteLine("{0}: {1}", i + 1, post.Title);
                Console.WriteLine("\t Created: {0}", post.PostDateTime);
                Console.WriteLine("\t Likes: {0} \t Dislikes: {1}", post.UpVoteCount, post.DownVoteCount);
                Console.WriteLine();
            }
            Console.WriteLine();
            var UserChoice = io.getInteger("Please select one of the following posts:", 1, posts.Count);
            Console.WriteLine();
            return posts[UserChoice - 1];
        }

        private void listNumberOfPosts()
        {
            Console.WriteLine();
            Console.WriteLine("{0} - Post available", PostList.NumberOfPosts());
        }

        private void ViewAllPosts()
        {
            var posts = PostList.FindAllPosts();

            var selectedPost = SelectPost(posts);
            PreviewPost(selectedPost);
            RatePost(selectedPost);
        }

        private void printPostByTitle()
        {
            Console.WriteLine();
            var post = getPostByTitleFromUser("Please enter the title of a post: ");
            Console.WriteLine();
            if (post != null)
            {
                PreviewPost(post);
                RatePost(post);
            }
            else
            {
            Console.WriteLine();
                Console.WriteLine("Could not find a post by that title...");
            }

        }

        private void RatePost(Post post)
        {
            Console.WriteLine();
            Console.WriteLine();
            var UserInput = io.getString("Want to rate the post, Y/N?: ");

            if (UserInput.Equals("Y", StringComparison.InvariantCultureIgnoreCase))
            {
            Console.WriteLine();
                Console.WriteLine("Press up to like and down to dislike: ");
            Console.WriteLine();
                var keyPress = Console.ReadKey();

                if (ConsoleKeyInfo.Equals(keyPress.Key, ConsoleKey.UpArrow))
                {
                    PostList.AddUpVote(post);
                }
                else if (ConsoleKeyInfo.Equals(keyPress.Key, ConsoleKey.DownArrow))
                {
                    PostList.AddDownVote(post);
                }
                else
                {
            Console.WriteLine();
            Console.WriteLine();
                    Console.WriteLine("No rating was posted");
                    Console.WriteLine("Press enter to continue...");
                    Console.ReadLine();
                }
            }
        }

        private void addPost()
        {

            Post post = new Post();

            Console.WriteLine();
            Console.WriteLine();
            post.Title = io.getString("Post Title: ");
            post.Description = io.getString("Post Content: ");

            PreviewPost(post);

            Console.WriteLine();
            Console.WriteLine();
            var approved = ApproveChanges("Save this post, Y/N?: ", "y");

            if (approved)
            {
                PostList.AddPost(post);
            }
            else
            {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
                Console.WriteLine("Post was discarded...");
                Console.WriteLine("Press enter to go back to the main menu...");
            }
        }

        private void PreviewPost(Post post)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(String.Format("Title: {0}", post.Title));
            Console.WriteLine(String.Format("Created On: {0}", post.PostDateTime));
            Console.WriteLine();
            Console.WriteLine(String.Format("Description: {0}", post.Description));
            Console.WriteLine();
            Console.WriteLine(String.Format("Likes: {0}   Dislikes: {1}", post.UpVoteCount, post.DownVoteCount));
            Console.WriteLine();
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();



        }

        private bool ApproveChanges(string message, string EqualitySetter)
        {
            var UserIput = io.getString(message);

            if (UserIput.Equals(EqualitySetter, StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }

            return false;
        }

        private void PrintMenu()
        {
            Console.WriteLine("1. Create a Post");
            Console.WriteLine("2. Find a Post by Title");
            Console.WriteLine("3. List all Posts");
            Console.WriteLine("4. remove a Post");
            Console.WriteLine("5. Quit");
        }
    }
}
