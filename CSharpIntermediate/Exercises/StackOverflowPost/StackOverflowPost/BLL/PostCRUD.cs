﻿using StackOverflowPost.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackOverflowPost.BLL
{
    class PostCRUD
    {
        private Dictionary<int, Post> PostList = new Dictionary<int, Post>();
        //This can be used for implementing fileIO for saving this list
        //private int Index;

        public void AddPost(Post post)
        {
            post.PostId = PostList.Count() + 1;
            post.PostDateTime = DateTime.Now;
            post.UpVoteCount = 0;
            post.DownVoteCount = 0;
            PostList.Add(post.PostId, post);

        }

        public void DeletePost(int postId)
        {
            PostList.Remove(postId);

        }

        public List<Post> FindPostByTitle(string title)
        {
            List<Post> FilteredPostsList = new List<Post>();
            foreach (var post in PostList)
            {
                string PostTitle = post.Value.Title;
                if (PostTitle.Length >= title.Length)
                {
                    string substring = PostTitle.Substring(0, title.Count());
                    if (PostTitle.ToUpper().Contains(title.ToUpper()))
                    {
                        FilteredPostsList.Add(post.Value);
                    }

                }
            }
            return FilteredPostsList;
        }

        public Post FindPostById(int id)
        {
            return PostList[id];
        }

        public int NumberOfPosts()
        {
            return PostList.Count();
        }

        public void AddUpVote(Post post)
        {
            PostList[post.PostId].UpVoteCount++;
        }

        public void AddDownVote(Post post)
        {
            PostList[post.PostId].DownVoteCount++;
        } 

        public List<Post> FindAllPosts()
        {
            return PostList.Values.ToList();
            
        }
    }
}
