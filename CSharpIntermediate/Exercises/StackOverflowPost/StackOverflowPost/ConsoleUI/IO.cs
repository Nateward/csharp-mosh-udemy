﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackOverflowPost.ConsoleUI
{
    public class IO
    {



        /**
         * Returns user input as an integer after given prompt.
         *
         * @param promptValue
         * @return user input value as integer
         */
        public int getInteger(String promptValue)
        {

            int inputValue = 0;

            bool exceptionCheck = true;

            while (exceptionCheck)
            {

                Console.WriteLine(promptValue);
                String inputString = Console.ReadLine();
                try
                {
                    inputValue = Convert.ToInt32(inputString);
                    exceptionCheck = false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Invalid input. Please type an integer.");
                    exceptionCheck = true;
                }
            }

            return inputValue;
        }

        /**
         * Returns user input as integer within min max range.
         *
         * @param promptValue
         * @param minValue
         * @param maxValue
         * @return user input value as integer
         */
        public int getInteger(String promptValue, int minValue, int maxValue)
        {
            int inputValue = 0;
            bool exceptionCheck = true;

            while (exceptionCheck)
            {

                Console.WriteLine(promptValue);
                String inputString = Console.ReadLine();

                try
                {
                    inputValue = Convert.ToInt32(inputString);

                    try
                    {
                        checkMinMaxInt(inputValue, minValue, maxValue);
                        exceptionCheck = false;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Invalid input. Integer must be between " + minValue + " and " + maxValue + ".");
                        exceptionCheck = true;
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Invalid input. Please type an integer.");
                    exceptionCheck = true;
                }
            }

            return inputValue;

        }

        /**
         * Throws exception if user input is out of range.
         *
         * @param inputValue
         * @param minValue
         * @param maxValue
         * @throws Exception
         */
        public void checkMinMaxInt(int inputValue, int minValue, int maxValue)
        {
            if (inputValue < minValue || inputValue > maxValue)
            {
                throw new Exception("Integer is outside of range.");
            }
        }

        /**
         * Returns user input as a String
         *
         * @param promptValue
         * @return user input as a String
         */
        public String getString(String promptValue)
        {

            Console.WriteLine(promptValue);
            return Console.ReadLine();

        }

        /**
         * Returns user input as a float after printing promptValue
         *
         * @param promptValue
         * @return user input as a float
         */
        public float getFloat(String promptValue)
        {
            float inputValue = 0.0f;

            bool exceptionCheck = true;

            while (exceptionCheck)
            {
                Console.WriteLine(promptValue);
                String inputString;
                inputString = Console.ReadLine();

                try
                {
                    inputValue = float.Parse(inputString);
                    exceptionCheck = false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Invalid type, please type in a float.");
                    exceptionCheck = true;

                }
            }

            return inputValue;

        }

        /**
         * Returns input as a float after printing promptValue and verifying it is between minValue and maxValue
         *
         * @param promptValue
         * @param minValue
         * @param maxValue
         * @return user input as a float
         */
        public float getFloat(String promptValue, float minValue, float maxValue)
        {
            float inputValue = 0.0f;

            bool exceptionCheck = true;

            while (exceptionCheck)
            {
                Console.WriteLine(promptValue);
                String inputString;
                inputString = Console.ReadLine();

                try
                {
                    inputValue = float.Parse(inputString);

                    try
                    {
                        checkMinMaxFloat(inputValue, minValue, maxValue);
                        exceptionCheck = false;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Invalid input. Float must be between " + minValue + " and " + maxValue + ".");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Invalid type, please type in a float.");
                    exceptionCheck = true;

                }
            }
            return inputValue;
        }

        /**
         * Throws exception if user input is out of range.
         *
         * @param inputValue
         * @param minValue
         * @param maxValue
         * @throws Exception
         */
        public void checkMinMaxFloat(float inputValue, float minValue, float maxValue)
        {
            if (inputValue < minValue || inputValue > maxValue)
            {
                throw new Exception("Float is outside of range.");
            }
        }

        /**
         * Returns user input as a double after printing promptValue
         *
         * @param promptValue
         * @return user input as a double
         */
        public double getDouble(String promptValue)
        {

            double inputValue = 0.0;

            bool exceptionCheck = true;

            while (exceptionCheck)
            {

                Console.WriteLine(promptValue);
                String inputString = Console.ReadLine();
                try
                {
                    inputValue = Convert.ToDouble(inputString);
                    exceptionCheck = false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Invalid input. Please enter value of double data type.");
                    exceptionCheck = true;
                }
            }

            return inputValue;

        }

        /**
         * Returns user input as a double after printing promptValue and ensuring it is between minValue and maxValue
         *
         * @param promptValue
         * @param minValue
         * @param maxValue
         * @return user input as a double
         */
        public double getDouble(String promptValue, double minValue, double maxValue)
        {

            double inputValue = 0;
            bool exceptionCheck = true;

            while (exceptionCheck)
            {

                try
                {
                    Console.WriteLine(promptValue);
                    String inputString = Console.ReadLine();
                    inputValue = Convert.ToDouble(inputString);

                    if (inputValue < minValue || inputValue > maxValue)
                    {
                        Console.WriteLine("Invalid input. Double value must be between " + minValue + " and " + maxValue + ".");
                        exceptionCheck = true;
                    }
                    else
                    {
                        exceptionCheck = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Invalid input. Please enter value of double data type.");
                    exceptionCheck = true;
                }
            }

            return inputValue;
        }

        /**
         * Prints a given promptValue to the console
         *
         * @param promptValue
         */
        public void print(String promptValue)
        {
            Console.WriteLine(promptValue);
        }

    }
}

