﻿using StackOverflowPost.Controller;


namespace StackOverflowPost
{
    class Program
    {
        static void Main(string[] args)
        {
            StackOverflowPostController controller = new StackOverflowPostController();
            controller.Run();
        }
    }
}

