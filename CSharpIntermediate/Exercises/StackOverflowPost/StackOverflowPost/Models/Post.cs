﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackOverflowPost.Model
{
    class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime PostDateTime { get; set; }
        public int UpVoteCount { get; set; }
        public int DownVoteCount { get; set; }

    }
}
