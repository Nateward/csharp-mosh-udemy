﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.CodeAlong
{
    class Test
    {
        public static void test1()
        {
            for (int i = 1; i <= 10; i++)
            {
                if (i % 2 == 1)
                {
                    Console.WriteLine(i);
                }

            }
            Console.ReadLine();
        }
    }
}

