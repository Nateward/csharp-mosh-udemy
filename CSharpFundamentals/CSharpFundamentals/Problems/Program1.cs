﻿using System;

namespace CSharpFundamentals.Problems
{
    public static class Program1
    {
        public static void Test1()
        {
            Console.WriteLine("Please enter a number between 1-10");
            int input1;
            Int32.TryParse(Console.ReadLine(), out input1);


            if (input1 > 0 && input1 < 11)
            {
                Console.WriteLine("Valid");
            }
            else
            {
                Console.WriteLine("Invalid");
            }
        }
    }
}
