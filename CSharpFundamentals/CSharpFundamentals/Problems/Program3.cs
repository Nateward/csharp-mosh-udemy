﻿using System;
using CSharpFundamentals.Problems;

namespace CSharpFundamentals.Problems
{
    public class Program3
    {
        public static void Test3()
        {
            Console.WriteLine("What is the width: ");
            var width = Program2.CompareIntegerValues();

            Console.WriteLine("\nWhat is the height: ");
            var height = Program2.CompareIntegerValues();

            if (width > height)
            {
                Console.WriteLine("\nThis image is a landscape image");
            }
            else
                Console.WriteLine("\nThis image is a portrait");

        }
    }
}