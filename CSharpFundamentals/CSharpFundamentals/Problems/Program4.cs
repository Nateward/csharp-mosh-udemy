﻿using System;

namespace CSharpFundamentals.Problems
{
    public class Program4
    {
        public static void Test4()
        {
            Console.WriteLine("Please enter the speed limit: ");
            var speedLimit = Program2.CompareIntegerValues();

            Console.WriteLine("\nPlease enter vehicle speed: ");
            var travelSpeed = Program2.CompareIntegerValues();

            if (travelSpeed < speedLimit)
            {
                Console.WriteLine("OK!");
            }
            else
                getDemerits(travelSpeed, speedLimit);
            Console.ReadLine();
        }

        private static void getDemerits(int travelSpeed, int speedLimit)
        {
            int count = 0;
            for (int i = speedLimit; i < travelSpeed; i = i + 5)
            {
                
                count++;
            }
            if (count > 12)
            {
                Console.WriteLine("Your License is suspended!");
            }
            else
                Console.WriteLine(@"{0} Point incurred on your license today.", count);
        }
    }
}