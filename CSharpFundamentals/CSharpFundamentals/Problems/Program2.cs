﻿using System;

namespace CSharpFundamentals.Problems
{
    public class Program2
    {
        public static void Test2()
        {
            Console.WriteLine("Here we will compare two number see which is larger \n");
            int input1 = CompareIntegerValues();
            

            int input2 = CompareIntegerValues();

            Console.WriteLine(@"{0} - Was the larger number ", input1 > input2 ? input1 : input2);
        }


        public static int CompareIntegerValues()
        {
            Console.Write("Please enter a number to compare... > ");
            int result;
            Int32.TryParse(Console.ReadLine(), out result);
            return result;
        }
    }
}