﻿using System;
using CSharpFundamentals.Problems;
using CSharpFundamentals.CodeAlong;
using CSharpFundamentals.Iterations;
namespace CSharpFundamentals
{
    class Program
    {
        static void Main(string[] args)
        { //Section 5, Lecture 42

            //Program1.Test1();

            //Program2.Test2();

            //Program3.Test3();

            //Program4.Test4();

            //string x = @"This is a long sentence for testing purposes. This is only a test. We have a new blog post up on our website right now. Come and view it. 
            //    If you like what you see please go ahead and hit the like button! We have more informatio on our website, so come on over and check us out.";

            //Console.WriteLine(SummaryString.summarizeText(x));

            //Console.WriteLine();

            //Console.WriteLine(SummaryString.summarizeText(x, 100));

            //Problem1.Test1();

            //Problem2.test2();

            //Problem3.test3();

            //Problem4.test4();

            //Problem5.test5();

            //ArraysLists.Exercise1.Test();
            //ArraysLists.Exercise2.Test();
            //ArraysLists.Exercise3.Test();
            //ArraysLists.Exercise4.Test();
            //ArraysLists.Exercise5.Test();


            //Strings.Exercise1.Test();
            //Strings.Exercise2.Test();
            //Strings.Exercise3.Test();
            //Strings.Exercise4.Test();
            //Strings.Exercise5.Test();

            PathFileDirectory.Exercise1.test();
            PathFileDirectory.Exercise2.test();

        }
    }
}
