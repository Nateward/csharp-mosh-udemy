﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.UILayer
{
    class UI
    {

        public static string getString(string message)
        {
            string userInput = "";
            while (String.IsNullOrWhiteSpace(userInput))
            {
                Console.WriteLine(message);
                userInput = Console.ReadLine();
            }
            return userInput;
        }
    }
}
