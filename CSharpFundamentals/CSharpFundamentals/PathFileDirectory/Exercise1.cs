﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.PathFileDirectory
{
    class Exercise1
    {
        public static void test()
        {
            var path = @"C:\Users\natew\Projects\CSharpUdemy\Mosh's\CSharpFundamentals";
            var fileExtension = "*.txt";

            var fileText = Exercise2.getFileText(path, fileExtension);

            var numberOfWords = getNumberOfWords(fileText);



            Console.WriteLine("The number of words in this file is : " + numberOfWords);
        }

        private static int getNumberOfWords(string fileText)
        {
            var words = fileText.Split(' ');

            return words.Count();
        }
    }
}
