﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.PathFileDirectory
{
    class Exercise2
    {
        public static void test()
        {
            var path = @"C:\Users\natew\Projects\CSharpUdemy\Mosh's\CSharpFundamentals";
            var fileExtension = "*.txt";

            var fileText = getFileText(path, fileExtension);

            var biggestWord = getLargestWord(fileText);



            Console.WriteLine("The Biggest word is: " + biggestWord);

        }

        private static string getLargestWord(string fileText)
        {
            var fileWords = fileText.Split(' ').ToList<string>();

            string biggestWord = "";

            foreach (var word in fileWords)
            {
                if (word.Length > biggestWord.Length)
                {
                    biggestWord = word;
                }
            }

            return biggestWord.Remove(biggestWord.IndexOf('.'));
        }

        public static string getFileText(string path, string fileExtension)
        {
            var files = Directory.GetFiles(path, fileExtension);
            var fileText = "";
            foreach (var file in files)
            {
                
                fileText = File.ReadAllText(file);

            }
            return fileText;
        }
    }
}
