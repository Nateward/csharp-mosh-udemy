﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.Iterations
{
    class Problem5
    {

        public static void test5()
        {
            Console.WriteLine("Please enter a series of numbers seperaated by a comma ie - 1, 4, 3, ...");
            string input = Console.ReadLine();


            input.Substring(0, input.Length);
            var numbers = input.Split(',');

            int max = Convert.ToInt32(numbers[0]);
            foreach (var str in numbers)
            {
                var number = Convert.ToInt32(str);
                if (number > max)
                {
                    max = number;
                }
            }

            Console.WriteLine("The highest number was {0}", max);
            Console.ReadLine();

        }
    }
}
