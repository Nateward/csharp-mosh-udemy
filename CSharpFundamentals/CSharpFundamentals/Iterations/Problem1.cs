﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.Iterations
{
    class Problem1
    {
        public static void Test1() {
            for (int i = 1; i < 100; i++)
            {
                if (i%3 == 0)
                {
                    Console.WriteLine(i);
                }
            }
            Console.ReadLine();
        }

    }
}
