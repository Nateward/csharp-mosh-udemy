﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.Iterations
{
    class Problem3
    {
        public static void test3()
        {
            Console.WriteLine("Please enter a number to be fatorized:");
            int input;
            int startNumber;

            Int32.TryParse(Console.ReadLine(), out startNumber);

            var factorizer = startNumber;
            input = startNumber - 1;
            for (int i = 0; i < input; i++)
            {
                
                factorizer = factorizer * (input - i);
            }

            Console.WriteLine(startNumber + "! = " + factorizer);

            Console.ReadLine();

        }
    }
}
