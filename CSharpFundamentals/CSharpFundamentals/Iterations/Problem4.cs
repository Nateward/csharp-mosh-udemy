﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.Iterations
{
    class Problem4
    {
        public static void test4()
        {
            Console.WriteLine("Guess A Random Number Game!!");
            var rng = new Random();
            int rnmdNumber = rng.Next(1, 10);
            int numberOfTries = 1;

            var input = getIntValue();

            while (true)
            {
                Console.WriteLine("The Number is: " + rnmdNumber);

                if (numberOfTries < 4)
                {

                    if (isGuessCorrect(input, rnmdNumber))
                    {
                        Console.WriteLine("You WIN!");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Wrong guess, try again...");
                        input = getIntValue();
                        numberOfTries++;
                    }
                }
                else
                {
                    Console.WriteLine("You Lost!");

                    break;
                }


            } 


            Console.WriteLine("Thanks for playing!!");
            Console.ReadLine();
        }

        public static int getIntValue()
        {
            int number = 0;
            while (!isValueBetween1and10(number))
            {
                Console.WriteLine("Please enter a number between 1 and 10: ");
                Int32.TryParse(Console.ReadLine(), out number);
            }
            return number;
        }

        public static bool isValueBetween1and10(int num1)
        {
            if (num1 > 0 && num1 <= 10)
            {
                return true;

            }

            return false;
        }

        public static bool isGuessCorrect(int a, int b)
        {
            return (a == b);
        }
    }
}
