﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.Iterations
{
    class Problem2
    {
        public static void test2()
        {
            string input = "";
            var result = 0;

            while (!string.Equals(input, "ok", StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("Please enter an number to add or press 'OK' to exit>>>");
                input = Console.ReadLine();
                try
                {
                    int intInput;
                    Int32.TryParse(input, out intInput);
                    result = result + intInput;
                }
                catch (Exception)
                {

                    throw;
                }

                Console.WriteLine("Total so far: " + result);
            }
            Console.WriteLine("Bye, have a beautiful time!");
            Console.ReadLine();
        }
    }
}
