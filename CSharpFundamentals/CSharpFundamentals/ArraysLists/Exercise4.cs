﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.ArraysLists
{
    class Exercise4
    {
        public static void Test()
        {
            List<int> numberList = new List<int>();
            string input = "";
            while (!string.Equals(input, "quit", StringComparison.InvariantCultureIgnoreCase))
            {
                input = getNumberFromUser();
                if (!string.Equals(input, "quit", StringComparison.InvariantCultureIgnoreCase))
                {
                    int numberValue;
                    Int32.TryParse(input, out numberValue);
                    numberList.Add(numberValue);
                }    
            }

            printList(numberList);

            List<int> newList = getUniqueNumbers(numberList);

            printList(newList);
            
        }

        private static List<int> getUniqueNumbers(List<int> numberList)
        {
            List<int> newList2 = new List<int>();
            for (int i = 0; i < numberList.Count; i++)
            {
                if (!newList2.Contains(numberList[i]))
                {
                    newList2.Add(numberList[i]);
                }
            }

            return newList2;
        }

        private static void printList(List<int> numberList)
        {
            foreach (var item in numberList)
            {
                Console.WriteLine(item);
            }
        }

        private static void printList()
        {
            throw new NotImplementedException();
        }

        private static string getNumberFromUser()
        {
            Console.WriteLine("Please enter a number or Quit to exit: ");
            string input  = Console.ReadLine();
            return input;
        }
    }
}
