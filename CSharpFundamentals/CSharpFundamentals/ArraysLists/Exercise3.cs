﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.ArraysLists
{
    class Exercise3
    {
        public static void Test()
        {
            int[] numbersArray = new int[5];

            for (int i = 0; i < numbersArray.Length; i++)
            {
                var number = getNumberFromUser();
                while (numbersArray.Contains(number))
                {
                    Console.WriteLine("ERROR: This number is already in the system!!!");
                    number = getNumberFromUser();
                }
                numbersArray[i] = number;

            }


            Console.WriteLine("OG Numbers:");
            foreach (var item in numbersArray)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            Array.Sort(numbersArray);

            Console.WriteLine("Sorted Numbers: ");
            foreach (var item in numbersArray)
            {
                Console.WriteLine(item);
            }
        }


        private static int getNumberFromUser()
        {
            Console.WriteLine("Please enter a number: ");
            int inputNumber;
            Int32.TryParse(Console.ReadLine(), out inputNumber);
            return inputNumber;
        }
    }
}
