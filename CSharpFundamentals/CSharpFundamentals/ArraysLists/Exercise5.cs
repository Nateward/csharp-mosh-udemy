﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.ArraysLists
{
    class Exercise5
    {
        public static void Test()
        {
           var input = getUserInput();

            List<int> numberList = splitStringIntoArray(input);

            
            while (numberList.Count < 5)
            {
                Console.WriteLine("Invalid List");
                input = getUserInput();
                numberList = splitStringIntoArray(input);
            }

            print3SmallestNumbers(numberList);
        }

        private static void print3SmallestNumbers(List<int> numberList)
        {
            numberList.Sort();

            Console.WriteLine("The 3 lowest numbers are: ");
            foreach (var item in numberList.Take(3))
            {
                Console.WriteLine(item);
            }
        }

        private static List<int> splitStringIntoArray(string input)
        {
            string[] numbers = input.Split(',');
            List<int> numberList = new List<int>();

            foreach (var item in numbers)
            {
                numberList.Add(Convert.ToInt32(item)); 
            }



            return numberList;
        }

        

        private static string getUserInput()
        {
            Console.WriteLine("Enter number seperated by a comma...>");
            string input = Console.ReadLine();

            return input;
        }
    }
}
