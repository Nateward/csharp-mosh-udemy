﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.ArraysLists
{
    class Exercise1
    {

        public static void Test()
        {
            var namesList = getNameList();

            printSwitch(namesList);


        }

        public static void printSwitch(List<string> namesList)
        {
            switch(namesList.Count)
            {
                case (int)NamesEnum.noNames:
                    {
                        Console.WriteLine("No one likes you!"); 
                    }
                    break;
                    case (int)NamesEnum.oneName:
                    {
                        Console.WriteLine(String.Format("{0} likes your post.", namesList.First().ToString()));
                    }
                    break;
                case (int)NamesEnum.twoNames:
                    {
                        Console.WriteLine(String.Format("{0} and {1} like your post.", namesList.First().ToString(), namesList[1].ToString()));
                    }
                    break;
                default:
                    {
                        Console.WriteLine(String.Format("{0} and {1} and [{2}] others like your post.",
                            namesList.First().ToString(),
                            namesList[1].ToString(),
                            namesList.Skip(2).Count()));
                    }
                    break;
            }
        }


        public static List<string> getNameList()
        {
            var nameList = new List<string>();
            while (true)
            {
                Console.Write("Please enter a name or press enter to move on... > ");
                string nameEntered = Console.ReadLine();
                if (!String.IsNullOrEmpty(nameEntered))
                {
                    nameList.Add(nameEntered);
                }
                else
                {
                    break;
                }
            }
            return nameList;
        }

    }

    public enum NamesEnum
    {
        noNames, oneName, twoNames
    }

}
