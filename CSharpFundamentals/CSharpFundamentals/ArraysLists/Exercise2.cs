﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.ArraysLists
{
    class Exercise2
    {
        internal static void Test()
        {
            var name = enterName();

            printReversedName(name);
        }

        private static void printReversedName(string name)
        {
            string output = new string(name.ToCharArray().Reverse().ToArray());
            Console.WriteLine("Reversed Name: " + output);
            
        }

        private static string enterName()
        {
            Console.WriteLine("Please enter your name....>");
            var name = Console.ReadLine();

            return name;

        }
    }

}
