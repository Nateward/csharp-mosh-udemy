﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSharpFundamentals.UILayer;

namespace CSharpFundamentals.Strings
{
    public class Exercise4
    {
        public static void Test()
        {
            var input = UI.getString("enter a few words separated by a space.");

            var words = input.ToLower().Split(' ');

            StringBuilder pascalCaseWords = new StringBuilder();

            foreach (var word in words)
            {
                pascalCaseWords.Append(word.Substring(0, 1).ToUpper())
                    .Append(word.Substring(1));

            }
            Console.WriteLine(pascalCaseWords.ToString());
        }
    }
}