﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.Strings
{
    class Exercise2
    {
        public static void Test()
        {
            var input = Exercise1.getUserInput();

            if (!string.IsNullOrWhiteSpace(input))
            {
                //var numbersList = ConvertToIntList(input);

                var isDuplicatesStatus = isDuplicates(input);

                printResult(isDuplicatesStatus);
            }
            
        }

        public static void printResult(bool status)
        {
            Console.WriteLine((status) ? "Duplicate Numbers" : "There are no Dups");
        }

        public static string getUserInput()
        {
            Console.WriteLine("PLease enter some numbers seperated by hyphens. ie - 1-2-3 or 7-3-4");
            var newString = Console.ReadLine();

            return newString;
        }
        

        private static bool isDuplicates(string input)
        {
            StringBuilder newString = new StringBuilder(input);
            newString.Replace("-", "");

            input = newString.ToString();
            for (int i = 0; i < input.Length; i++)
            {
                if (input.Substring(i + 1).Contains(input[i]) && i != input.Length)
                {
                    return true;
                }
            }
           
            return false;
        }
    }
}
