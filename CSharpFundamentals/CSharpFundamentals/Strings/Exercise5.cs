﻿using CSharpFundamentals.UILayer;
using System;
using System.Collections.Generic;

namespace CSharpFundamentals.Strings
{
    public class Exercise5
    {
        public static void Test()
        {
            var input = UI.getString("enter an English word");

            

            string vowels =  "aeiou";

            List<string> vowelsList = new List<string>();

            foreach (char letter in input)
            {
                
                
                    if (vowels.Contains(letter.ToString()))
                    {
                        vowelsList.Add(letter.ToString());
                    }
                
            }

            Console.WriteLine(vowelsList.Count);
        }
    }
}