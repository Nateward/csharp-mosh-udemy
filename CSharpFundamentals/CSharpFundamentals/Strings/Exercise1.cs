﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.Strings
{
    class Exercise1
    {
        public static void Test()
        {
            var inputString = getUserInput();

            var stringArray = ConvertToArray(inputString);

            var numbersArray = convertToNumbersArray(stringArray);

            var checkedNumbers = areNummberConsecutive(numbersArray);

            printResult(checkedNumbers);
        }

        public static void printResult(bool status)
        {
            Console.WriteLine((status)? "Consecutive Numbers" : "This are non-consecutive numbers");
        }

        public static bool areNummberConsecutive(int[] numbersArray)
        {
            bool status = true;

            Array.Sort(numbersArray);

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (i != numbersArray.Length - 1)
                {
                    if (numbersArray[i] != (numbersArray[i + 1] - 1))
                    {
                        status = false;
                    }
                }
                
            }
            return status;
        }

        public static string[] ConvertToArray(string inputString)
        {
            return inputString.Split('-').ToArray();
        }

        public static int[] convertToNumbersArray(string[] stringArray)
        {
            int[] numbersArray = new int[stringArray.Length];

            for (int i = 0; i < stringArray.Length; i++)
            {
                numbersArray[i] = Convert.ToInt32(stringArray[i]);
            }

            return numbersArray;
        }

        public static string getUserInput()
        {
            Console.WriteLine("PLease enter some numbers seperated by hyphens. ie - 1-2-3 or 7-3-4");
            var newString = Console.ReadLine();

            return newString;
        }
    }
}
