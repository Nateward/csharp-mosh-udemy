﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpFundamentals.Strings
{
    class Exercise3
    {
        internal static void Test()
        {
            var input = getUserIput();

            bool validTime = checkTimeFormat(input);

            Console.WriteLine((validTime)? "Ok" : "Invalid Time");
        }

        private static bool checkTimeFormat(string input)
        {
            try
            {
                var newTimeSpan = TimeSpan.Parse(input);
            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }

        private static string getUserIput()
        {
            Console.WriteLine("enter a time value in the 24-hour time format (e.g. 19:00)");

           return Console.ReadLine();
        }
    }
}
